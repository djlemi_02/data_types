console.log('Hello World')
// 
/*


*/

// Identifiers
// Variables and Constants
// Variables = Can be changed. These are identifiers whose value can change. 
// To use them, we use the word 'let'
let name = 'DJ Lemi'
console.log(name)
name = 'JL Lemi'
console.log(name)
// vs. constants
// We use the keyword const for constants
const school = 'La Salle'
// school = 'UST'

// VARIABLES

// Declaration
// Declaration of variables
// Sinulat pero hindi pa ginamit
// Declared, but not used. 
let food;

// Initialization
// The initial value of a variable has been declared
let drink = 'coke';
// May inilagay nang nauna

// Reassignment
drink = 'Soda'
// The value has been changed for the variable. 
food = 'Spaghetti'
console.log(drink)
console.log(food)


// JavaScript DATA TYPES

// 1. String
// Strings are texts. 
// They are enclosed with '', "", ``
// They are enclosed with single quotation marks, double quotations marks, and backticks
let highschool = "UST"
let elementary = `Vigan Central School`
let id_number = '123123'

// 2. Number
let gcash = 8451254;
let planetSize = 123e10;
let cold = -100; 

// 3. Boolean
let lying = true; 
let Filipino = false;

// 4. Null
// Null is assigned
let entry = null;

// 5. Undefined
// Undefined is not. 
let entry_again;

console.log(entry, entry_again)

// 6. Objects
// Objects can contain strings, numbers, boolean, and other objects
// Objects: Array & Object Literal

// Array
let grades = [90, 91, 95, 96]

// Object Literal
let gradings_grade = {
    first_grading: 91, 
    second_grading: 93,
    third_grading: 97, 
    fourth_grading: 100
}

